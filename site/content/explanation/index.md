---
title: "Explanation"
date: 2022-10-05T16:12:48+02:00
draft: false
---

This project is a way for me to train how to design and build CI/CD-pipelines. It consists of a website (the one you are currently looking at) and a pipeline, which facilitates the development by automating most of the operational tasks.

![pipeline](/pipeline.svg)

The centerpiece of the architecture is [GitLab](https://about.gitlab.com/), as on the one hand it hosts the repository and on the other hand it runs the different stages of the CI/CD-pipeline. Every time code is pushed to the repository (1), GitLab triggers the pipeline (2). The first stage handles the building of the website, but before that happens, GitLab pulls an image from a container registry (3). GitLab will then execute all the steps from the first stage inside this container. This will happen for each stage, because they all have their own image. The website is built with [Hugo](https://gohugo.io/), a static site generator, and in the first stage Hugo takes multiple Markdown files and a design template and renders them into the finished website files (4). These files are saved as an artifact, so they can be used during later stages. The second stage takes care of testing (5), by executing a Python script. This script uses the [Requests](https://requests.readthedocs.io/) and [Beautiful Soup 4](https://beautiful-soup-4.readthedocs.io/) libraries, and verifies that all the links are working properly. This stage doesn't represent a complete test suite, as it exists mostly to showcase how the integration of tests in a CI/CD-pipeline looks like. After that, we use [Terraform](https://www.terraform.io/) to provision two [DigitalOcean droplets](https://www.digitalocean.com/products/droplets/) and attach domains to them (6). The state file is managed by GitLab, which supports state-locking to prevent concurrent runs of Terraform against the same state. In the last stage, the droplets are then configured using [Ansible](https://www.ansible.com/) (7). For the first droplet, which hosts the website, this consists of setting up [Caddy](https://caddyserver.com/) and copying the website files from the first stage to the server (8), as well as setting up a node exporter. On the second droplet, which is used to monitor the first droplet, Ansible sets up [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/). Prometheus will scrape metrics about the first droplet, which are provided by the node exporter, and Grafana can access that data and visualize it in a dashboard.

The project is still a work in progress and will be updated in the future with the following, among others:
- reduction of the size of the container images to improve pipeline efficiency
- using multiple environments in the CI/CD process, e.g. staging and production
- caching to avoid downloading the same stuff over and over
- new features for the website