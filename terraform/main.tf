terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/40258311/terraform/state/main_state"
    lock_address = "https://gitlab.com/api/v4/projects/40258311/terraform/state/main_state/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/40258311/terraform/state/main_state/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = "5"
    username = "gitlab_token"
  }
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {
	token = var.do_token
}

data "digitalocean_ssh_key" "gitlab_key" {
  name = "gitlab_key"
}

# website server

resource "digitalocean_droplet" "website" {
  image  = "debian-11-x64"
  name   = "demo-website"
  region = "fra1"
  size   = "s-1vcpu-512mb-10gb"
  ssh_keys = [
    data.digitalocean_ssh_key.gitlab_key.id
  ]
}

resource "digitalocean_domain" "domain_1" {
  name = "bonert.me"
  ip_address = digitalocean_droplet.website.ipv4_address
}

resource "digitalocean_domain" "domain_2" {
  name = "www.bonert.me"
  ip_address = digitalocean_droplet.website.ipv4_address
}

# monitoring server

resource "digitalocean_droplet" "monitoring" {
  image  = "debian-11-x64"
  name   = "demo-monitoring"
  region = "fra1"
  size   = "s-1vcpu-512mb-10gb"
  ssh_keys = [
    data.digitalocean_ssh_key.gitlab_key.id
  ]
}

resource "digitalocean_domain" "domain_3" {
  name = "grafana.bonert.me"
  ip_address = digitalocean_droplet.monitoring.ipv4_address
}