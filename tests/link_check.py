import urllib

import requests
from bs4 import BeautifulSoup



base_url = "http://localhost:1313/"
relative_list = ["", "/explanation/"]

print("\n######################## TEST START ########################\n")

for index, relative_path in enumerate(relative_list):
	website = urllib.parse.urljoin(base_url, relative_path)
	response = requests.get(website)
	link_list = BeautifulSoup(response.text, "html.parser").find_all("a")

	print(f"website {index + 1}/{len(relative_list)}: {website}")

	for index, link in enumerate(link_list):
		url = link.get("href")
		url = urllib.parse.urljoin(base_url, url)
		print(f"\tlink {index + 1}/{len(link_list)}: {url}")
		if ((url == None) or (url == "")):
			print("\t\turl not configured or empty")
			continue
		response = requests.get(url)
		assert response.status_code == 200
		print("\t\turl is okay")

print("\n######################### TEST END #########################\n")